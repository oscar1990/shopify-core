# How to Set Up Shopify

## Step 1: Getting started with Theme Kit

https://shopify.dev/themes/tools/theme-kit/getting-started

## Step 2: Theme get

a. Tạo folder để get code về (Example: shopify-core)

b. Tạo 2 file trong folder "shopify-core":
config.yml
.gitignore

b.1. Nội dung file config.yml bao gồm: password của private apps đã tạo, id của theme, tên domain

development:
  password: 40203525eb3f722c3ba18a62f5fa2572
  theme_id: "120323964973"
  store: spotire-oralie.myshopify.com
  ignore_files:
  - config/settings_data.json

b.2. Nội dung file .gitignore

settings_data.json
.idea

## Step 3: Theme watch --allow-live and theme deploy

Sử dụng lệnh "theme watch --allow-live" để xem trực tiếp sự thay đổi khi phát triển site
Sử dụng lệnh "theme deploy" để đẩy 1 file thay đổi hay nhiều file lên server